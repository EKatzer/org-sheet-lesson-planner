#+title: Changelog

** [[https://docs.google.com/spreadsheets/d/1ItPj1_pGHRVgV7ZFvnv7dcZc79P5_4_mvoOm8ta2YJ8/copy][v0.4]]
- [X] Changing the list of classes will no longer modify previous entries/days
- [X] Bugfix: Filter views should correctly color different classes
- [X] Entry types should be subject-agnostic, and the user can now change the names of the entry types (Assignment, Presentation, etc...)
- [X] Submenus to auto create and link new files should be editable, via the Template Registry
  - [X] Submenus should allow users to toggle ability to create Google Docs, Google Sheets, Google Slides, and Google Forms
  - [X] Submenus should allow users to create from template files with user-defined names
** [[https://docs.google.com/spreadsheets/d/1GGASB0TpQsaqe8vViT5emGZJ7u6ZJC4scAtKuUN0A7s/copy][v0.3]]
- [X] Automatically group entries/classes/days via grouped rows: this allows folding
- [X] Create basic submenus in the top UI which allow you to automatically /create and link/ a new file to the new entry
** [[https://docs.google.com/spreadsheets/d/1lVfxMikSjv_njqfMenIc85AiQx3I9ZQ71iQEyc2f4aw/copy][v0.2]]
- [X] Automatically store spreadsheet file link in Manifest upon copying
- [X] Automatically pull user's email
- [X] A dashboard for first-time setup will be added
** [[https://docs.google.com/spreadsheets/d/1mWT6B5dN5kKugbiTPpsToZNGq1kg84RsrzyWe4q6RB8/copy][v0.1]]
- [X] The first class will no longer have formatting errors under certain conditions
- [X] Entries can now be dragged to different days without messing up formatting
- [X] You can now change default classes, as well as add or subtract them!
  - [X] A new "ClassRegistry" sheet was made for this
- [X] The "O" todo-item was added (for "Organize file")
- [X] The "Comment" entry type was added, which allows you to create an entry without todo-items
** v0.0
Note: This lacks essential functionality of quickly changing the lists of classes, and is not generally usable without manually fixing it.
- [X] The spreadsheet performs basic essential functions:
  - [X] Allows you to add new days and new entries via a submenu
  - [X] Applies conditional formatting to entries/days when they are marked complete
  - [X] Has filter view sheets:
    - [X] One is a list of all incomplete items
    - [X] One is a list of ungraded items
  - [X] There are embedded functions which can be set as triggers for auto-emailing yourself reminders from the filter views
    - [X] todoListEmailReminder()
    - [X] gradingListEmailReminder()
