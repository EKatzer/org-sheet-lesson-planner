/** @NotOnlyCurrentDoc */

// Function which populates a new day template in first available line on "Daily Entries"
// Utilizes template sheet named "DayTemplate"
function NewDay() {
  // Retrieve the current spreadsheet
  var spreadsheet = SpreadsheetApp.getActive();

  // Retrieve the "Daily Entries" sheet
  var sheet = spreadsheet.getSheetByName("Daily Entries");

  // Manifest!A2 is set to give the number of non-empty rows in "Daily Entries"
  var i = spreadsheet.getRange("Manifest!A2").getValue();

  // ClassRegistry!A1 is set to give the number of classes
  // This is used to clip the correct amount of rows from the Day Template
  var numClasses = spreadsheet.getRange("ClassRegistry!A1").getValue();
  Logger.log(numClasses);

  // Find first blank row and paste the new day template there
  var pastingRange = spreadsheet.getRange("Daily Entries!A" + String(i + 1));
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName("Daily Entries"), true);
  spreadsheet
    .getRange("DayTemplate!A1:I" + String(numClasses + 1))
    .copyTo(pastingRange, SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);

  // Find range to group
  var groupingRange = spreadsheet.getRange(
    "Daily Entries!" + String(i + 2) + ":" + String(i + 2 + numClasses - 1)
  );

  // Group classes underneath the current day
  groupingRange.shiftRowGroupDepth(1);

  // Makes every cell in column A a value instead of a function (keeps class names at entry creation, instead of changing class names)
  sheet
    .getRange("A:A")
    .copyTo(
      sheet.getRange("A:A"),
      SpreadsheetApp.CopyPasteType.PASTE_VALUES,
      false
    );
}

// Function which adds a new entry based on pre-defined templates
// Templates should be in similar format to DayTemplate, and should be named in form:
// EntryNTemplate, where N is a number 1-9
// Function also can create a file of the specified name, and add it as a link to the entry
// Accepted file types are: docs, sheets, slides, forms, and an id to a Google drive file to be copied as a template
// To ignore making a linked file, pass "" for the fileType
function AddEntry(entryType, fileType) {
  // Retrieve the current spreadsheet
  var spreadsheet = SpreadsheetApp.getActive();

  // Retrieve the "Daily Entries" sheet
  var sheet = spreadsheet.getSheetByName("Daily Entries");

  // Retrieve current row number, and set focus to column A
  var currentCellRow = sheet.getActiveRange().getRowIndex();
  var currentRange = sheet.getRange(currentCellRow, 1);
  sheet.setActiveRange(currentRange);

  // Retrieve value of column A
  var currentCategory = currentRange.getValue();

  // Add blank row in order to copy entry template in
  spreadsheet.getActiveSheet().insertRowsAfter(currentCellRow, 1);

  // Locate appropriate row inside entry template sheet for copying
  var i = 2;
  var found = false;
  while (true) {
    Logger.log(
      spreadsheet
        .getRange("Entry" + String(entryType) + "Template!A" + i)
        .getValue()
    );
    if (
      spreadsheet
        .getRange("Entry" + String(entryType) + "Template!A" + i)
        .getValue() == currentCategory
    ) {
      break;
    }
    i = i + 1;
  }

  // Paste relevant row from entry template sheet into "Daily Entries"
  spreadsheet
    .getRange(
      "Entry" + String(entryType) + "Template!" + String(i) + ":" + String(i)
    )
    .copyTo(
      spreadsheet.getActiveRange().offset(1, 0),
      SpreadsheetApp.CopyPasteType.PASTE_NORMAL,
      false
    );

  // Focus the new entry name
  spreadsheet.getActiveRange().offset(1, 1).activate();

  // Get depth of current row
  var currentRow = spreadsheet.getActiveRange().getRow();
  var currentGroupDepth = spreadsheet
    .getSheetByName("Daily Entries")
    .getRowGroupDepth(currentRow);

  // If the current group depth is not 2, shift the group depth
  // Allows folding underneath classes
  if (currentGroupDepth < 2) {
    // Group the new entry name one level below the class
    spreadsheet.getActiveRange().shiftRowGroupDepth(1);
  }

  var ui = SpreadsheetApp.getUi();
  var fileNameResponse = ui.prompt(
    "Entry Name",
    "What is the name of this entry for " +
      String(spreadsheet.getActiveRange().offset(0, -1).getValue()) +
      "?",
    ui.ButtonSet.OK_CANCEL
  );

  var newFile;
  var fileLink;

  if (fileNameResponse.getSelectedButton() === ui.Button.OK) {
    if (fileType === "docs") {
      newFile = DocumentApp.create(fileNameResponse.getResponseText());
    } else if (fileType === "slides") {
      newFile = SlidesApp.create(fileNameResponse.getResponseText());
    } else if (fileType === "sheets") {
      newFile = SpreadsheetApp.create(fileNameResponse.getResponseText());
    } else if (fileType === "forms") {
      newFile = FormApp.create(fileNameResponse.getResponseText());
    } else if (fileType !== "") {
      newFile = DriveApp.getFileById(fileType).makeCopy(
        fileNameResponse.getResponseText()
      );
    }

    if (fileType !== "") {
      if (fileType === "forms") {
        fileLink = newFile.getEditUrl();
      } else {
        fileLink = newFile.getUrl();
      }
      SpreadsheetApp.getActiveRange().setFormula(
        '=HYPERLINK("' +
          fileLink +
          '","' +
          fileNameResponse.getResponseText() +
          '")'
      );
    } else {
      SpreadsheetApp.getActiveRange().setValue(
        fileNameResponse.getResponseText()
      );
    }

    // Makes every cell in column A a value instead of a function (keeps class names at entry creation, instead of changing class names)
    sheet
      .getRange("A:A")
      .copyTo(
        sheet.getRange("A:A"),
        SpreadsheetApp.CopyPasteType.PASTE_VALUES,
        false
      );
  }
}

function AddEntrywithTemplate(entryType, templateNum) {
  var spreadsheet = SpreadsheetApp.getActive();

  // Access rows and columns via templateRegistry[row-2][column-1]
  var templateRegistry = spreadsheet
    .getRange("TemplateRegistry!A2:N10")
    .getValues();

  var colA = 0;
  var colB = 1;
  var colC = 2;
  var colD = 3;
  var colE = 4;
  var colF = 5;
  var colG = 6;
  var colH = 7;
  var colI = 8;
  var colJ = 9;
  var colK = 10;
  var colL = 11;
  var colM = 12;
  var colN = 13;

  var templateFile1 = colG;
  var templateFile1Name = colH;

  var templateFile2 = colI;
  var templateFile2Name = colJ;

  var templateFile3 = colK;
  var templateFile3Name = colL;

  var templateFile4 = colM;
  var templateFile4Name = colN;

  var addColumns = (templateNum - 1) * 2;

  var templateFileId =
    templateRegistry[entryType - 1][templateFile1 + addColumns];

  AddEntry(entryType, templateFileId);
}

// Wrapper functions to add different types of entries

function AddEntry1() {
  AddEntry(1, "");
}

function AddEntry1withDocs() {
  AddEntry(1, "docs");
}

function AddEntry1withSlides() {
  AddEntry(1, "slides");
}

function AddEntry1withSheets() {
  AddEntry(1, "sheets");
}

function AddEntry1withForms() {
  AddEntry(1, "forms");
}

function AddEntry1withTemplate1() {
  AddEntrywithTemplate(1, 1);
}

function AddEntry1withTemplate2() {
  AddEntrywithTemplate(1, 2);
}

function AddEntry1withTemplate3() {
  AddEntrywithTemplate(1, 3);
}

function AddEntry1withTemplate4() {
  AddEntrywithTemplate(1, 4);
}

function AddEntry2() {
  AddEntry(2, "");
}

function AddEntry2withDocs() {
  AddEntry(2, "docs");
}

function AddEntry2withSlides() {
  AddEntry(2, "slides");
}

function AddEntry2withSheets() {
  AddEntry(2, "sheets");
}

function AddEntry2withForms() {
  AddEntry(2, "forms");
}

function AddEntry3() {
  AddEntry(3, "");
}

function AddEntry3withDocs() {
  AddEntry(3, "docs");
}

function AddEntry3withSlides() {
  AddEntry(3, "slides");
}

function AddEntry3withSheets() {
  AddEntry(3, "sheets");
}

function AddEntry3withForms() {
  AddEntry(3, "forms");
}

function AddEntry4() {
  AddEntry(4, "");
}

function AddEntry5() {
  AddEntry(5, "");
}

function AddEntry6() {
  AddEntry(6, "");
}

function AddEntry7() {
  AddEntry(7, "");
}

function AddEntry8() {
  AddEntry(8, "");
}

function AddEntry9() {
  AddEntry(9, "");
}

// Generate the "My Planner" UI menu (next to the "Help" UI menu)
function onOpen() {
  var spreadsheet = SpreadsheetApp.getActive();

  // Access rows and columns via templateRegistry[row-2][column-1]
  var templateRegistry = spreadsheet
    .getRange("TemplateRegistry!A2:N10")
    .getValues();

  var colA = 0;
  var colB = 1;
  var colC = 2;
  var colD = 3;
  var colE = 4;
  var colF = 5;
  var colG = 6;
  var colH = 7;
  var colI = 8;
  var colJ = 9;
  var colK = 10;
  var colL = 11;
  var colM = 12;
  var colN = 13;
  var template1 = 0;
  var template2 = 1;
  var template3 = 2;
  var template4 = 3;
  var template5 = 4;
  var template6 = 5;
  var template7 = 6;
  var template8 = 7;
  var template9 = 8;

  var templateNum = colA;
  var templateName = colB;
  var createWithDocs = colC;
  var createWithSlides = colD;
  var createWithSheets = colE;
  var createWithForms = colF;

  var templateFile1 = colG;
  var templateFile1Name = colH;

  var templateFile2 = colI;
  var templateFile2Name = colJ;

  var templateFile3 = colK;
  var templateFile3Name = colL;

  var templateFile4 = colM;
  var templateFile4Name = colN;

  var cols = [
    colA,
    colB,
    colC,
    colD,
    colE,
    colF,
    colG,
    colH,
    colI,
    colJ,
    colK,
    colL,
    colM,
    colN,
  ];

  var templates = [
    template1,
    template2,
    template3,
    template4,
    template5,
    template6,
    template7,
    template8,
    template9,
  ];

  var submenus = [];

  var ui = SpreadsheetApp.getUi();

  for (let i = 0; i < templates.length; i++) {
    if (templateRegistry[i][templateName] === "") {
      break;
    }
    if (templateRegistry[i][templateName] !== "") {
      newSubmenu = ui.createMenu(templateRegistry[i][templateName]);

      newSubmenu.addItem("without New File", "AddEntry" + String(i + 1));

      if (templateRegistry[i][createWithDocs]) {
        newSubmenu.addItem(
          "with Google Doc",
          "AddEntry" + String(i + 1) + "withDocs"
        );
      }

      if (templateRegistry[i][createWithSlides]) {
        newSubmenu.addItem(
          "with Google Slides",
          "AddEntry" + String(i + 1) + "withSlides"
        );
      }

      if (templateRegistry[i][createWithSheets]) {
        newSubmenu.addItem(
          "with Google Sheets",
          "AddEntry" + String(i + 1) + "withSheets"
        );
      }

      if (templateRegistry[i][createWithForms]) {
        newSubmenu.addItem(
          "with Google Forms",
          "AddEntry" + String(i + 1) + "withForms"
        );
      }

      for (let j = 0; j <= 4 * 2; j = j + 2) {
        if (
          (templateRegistry[i][templateFile1 + j] !== "") *
          (templateRegistry[i][templateFile1Name + j] !== "")
        ) {
          newSubmenu.addItem(
            "with " + templateRegistry[i][templateFile1Name + j],
            "AddEntry" + String(i + 1) + "withTemplate" + String(j / 2 + 1)
          );
        } else {
          break;
        }
      }

      submenus.push(newSubmenu);
    }
  }

  var menu = ui
    .createMenu("My Planner")
    .addItem("Add new day", "NewDay")
    .addSeparator()
    .addSubMenu(
      ui
        .createMenu("Add new entry")
        .addItem("Comment", "AddEntry9")
        .addSubMenu(submenus[0])
        .addSubMenu(submenus[1])
        .addSubMenu(submenus[2])
        .addItem("Todo Item", "AddEntry8")
    )
    .addToUi();
}

// Function to parse a filtered list (todo or grading list) and send email
// The email is sent to whatever email address is defined in Manifest!A3
// The link included in the email also depends on the spreadsheet url, which is defined in Manifest!A1
function EmailReminder(listSheetName, emailSubject, emailBodyTitle) {
  // Retrieve the current spreadsheet
  var spreadsheet = SpreadsheetApp.getActive();

  // Retrieve email from Manifest!A3
  var yourEmail = Session.getActiveUser().getEmail();

  var thisPlannerUrl =
    "https://docs.google.com/spreadsheets/d/" +
    SpreadsheetApp.getActive().getId() +
    "/edit";

  // Retrieve the relevant filtered list sheet by name
  var sheet = spreadsheet.getSheetByName(listSheetName);

  // Store todoItems in a 2-dimensional array
  // Values accessed as todoItemArray[i][j]
  // i is the todo item number (row)
  // j can retrieve the column, which can retrieve name of the todo item, and which class the todo item is in
  var todoItemArray = sheet.getRange("A:C").getValues();

  // enumerate integers to be used as the "j" in todoItemArray[i][j]
  var colA = 0;
  var colB = 1;
  var colC = 2;

  // Declare email html string
  var emailHTML;

  // Create html and body root, plus email body title
  emailHTML = "<html><body><h2>" + emailBodyTitle + "</h2>";

  // For all values in the todo item array
  for (let i = 0; i < todoItemArray.length; i++) {
    // Exit this loop if column B is blank
    if (String(todoItemArray[i][colB]) == "") {
      break;
    }

    // If column A is false, this is a subheading (name of class)
    else if (todoItemArray[i][colA] == false) {
      emailHTML = emailHTML + "<h3>" + todoItemArray[i][colB];
    }

    // Otherwise, it is a todo-item entry
    else {
      // If the previous entry was a subheading, start an unordered list
      if (i > 0) {
        if (todoItemArray[i - 1][colA] == false) {
          emailHTML = emailHTML + "<ul>";
        }
      }

      // Add the list items to the html
      emailHTML =
        emailHTML +
        "<li>" +
        todoItemArray[i][colB] +
        "←" +
        todoItemArray[i][colA] +
        "</li>";
    }

    // Check if the next item is a subheading or blank, in which case, close the unordered list
    if (i < todoItemArray.length - 1) {
      if (todoItemArray[i + 1][colA] == false) {
        emailHTML = emailHTML + "</ul>";
      }
    }
  }

  // Include link to this planer at the bottom of the email
  emailHTML =
    emailHTML +
    "<p>Open your planner for more details: " +
    thisPlannerUrl +
    "</p>";

  // Close html and body root
  emailHTML = emailHTML + "</html></body>";

  // Send the email to the email address defined above (from Manifest!A3)
  MailApp.sendEmail(yourEmail, emailSubject, "placeholder", {
    htmlBody: emailHTML,
  });
}

// Wrapper email reminder functions

function todoListEmailReminder() {
  EmailReminder(
    "READ-ONLY Todo List",
    "Todo List for Today!",
    "Here's your todo list for today:"
  );
}

function gradingListEmailReminder() {
  EmailReminder(
    "READ-ONLY Grading List",
    "Grading reminder!",
    "Here's your grading list for this weekend:"
  );
}

function ThisFileId() {
  return SpreadsheetApp.getActive().getId();
}
